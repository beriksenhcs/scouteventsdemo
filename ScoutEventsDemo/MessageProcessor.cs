﻿using System;
using System.Threading.Tasks;
using HCS.Scout.Core.Events;
using HCS.Scout.Core.Events.Aws;
using Microsoft.Extensions.Logging;

namespace ScoutEventsDemo
{
    public class MessageProcessor : IMessageProcessor
    {
        private readonly ILogger<MessageProcessor> _logger;
        private readonly IEventDeserializer _eventDeserializer;
        private readonly IEventIdentityProvider _scoutEventIdentityProvider;
        private readonly IEventCorrelationIdProvider _eventCorrelationIdProvider;

        private static readonly Action<ILogger, string, Exception> _skippingUnhandledEvent =
             LoggerMessage.Define<string>(LogLevel.Information, new EventId(100, "SkippingUnhandledEvent"),
                 "Skipping event. No handler implemented for type {type}.");
        private static readonly Action<ILogger, string, int, Exception> _eventLog =
             LoggerMessage.Define<string, int>(LogLevel.Information, new EventId(101, "EventLog"),
                 "Received Event {someString} {someInt}.");

        public MessageProcessor(ILogger<MessageProcessor> logger, IEventDeserializer eventDeserializer,
            IEventIdentityProvider scoutEventIdentityProvider, IEventCorrelationIdProvider eventCorrelationIdProvider)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventDeserializer = eventDeserializer ?? throw new ArgumentNullException(nameof(eventDeserializer));
            _scoutEventIdentityProvider = scoutEventIdentityProvider ?? throw new ArgumentNullException(nameof(scoutEventIdentityProvider));
            _eventCorrelationIdProvider = eventCorrelationIdProvider ?? throw new ArgumentNullException(nameof(eventCorrelationIdProvider));
        }

        public Task ProcessMessage(string message)
        {
            // Deserialize the event into the standard event object.
            var cloudEvent = _eventDeserializer.Deserialize(message);

            // Extract some info about the event, if needed.
            var eventInfo = cloudEvent.Extension<EventInfoExtension>();

            // Sets the correlation id for everything inside and also appends it to the log statements.
            using (_eventCorrelationIdProvider.SetCorrelationId(cloudEvent, _logger))
            // Sets the identity for everything inside and also appends it to the log statements.
            using (_scoutEventIdentityProvider.SetIdentity(cloudEvent, _logger))
            using (_logger.BeginScope("{originalTimestamp}", cloudEvent.Time.Value.ToString("s")))
            {
                switch (cloudEvent.Type.ToLower())
                {
                    case ("hcs:braden:myaggregate:myeventtype:v1"):
                        var extractedEvent = cloudEvent.ExtractData<MyDomainEvent>();
                        //do something with the event, most likely through mediatr.
                        _eventLog(_logger, extractedEvent.SomeString, extractedEvent.SomeInt, null);
                        break;
                    default:
                        _skippingUnhandledEvent(_logger, cloudEvent.Type, null);
                        break;
                }
            }
            return Task.CompletedTask;
        }
    }
}
