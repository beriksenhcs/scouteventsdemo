﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HCS.Scout.Core.Events;
using MediatR;
using Microsoft.Extensions.Hosting;

namespace ScoutEventsDemo
{
    /// <summary>
    /// A simple service that produces a test event. The event is handed off to Mediatr to handle, which is done in the HCS.Scout.Core.Events.Aws package.
    /// </summary>
    /// <seealso cref="Microsoft.Extensions.Hosting.BackgroundService" />
    public class Producer : BackgroundService
    {
        private readonly IEventFactory _eventFactory;
        private readonly IMediator _mediator;

        public Producer(IEventFactory eventFactory, IMediator mediator)
        {
            _eventFactory = eventFactory ?? throw new ArgumentNullException(nameof(eventFactory));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var e = _eventFactory.Create("MyAggregate", "MyEventType", new MyDomainEvent { SomeInt = 7, SomeString = "my string" });
            await _mediator.Publish(e);
        }
    }

    public class MyDomainEvent
    {
        public string SomeString { get; set; }
        public int SomeInt { get; set; }
    }
}
