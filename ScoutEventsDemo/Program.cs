﻿using System;
using System.IO;
using System.Threading.Tasks;
using Amazon.SimpleNotificationService;
using Amazon.SQS;
using HCS.Scout.Core.CorrelationId;
using HCS.Scout.Core.CorrelationId.Serilog;
using HCS.Scout.Core.Events;
using HCS.Scout.Core.Events.Aws;
using HCS.Scout.Core.Identity;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace ScoutEventsDemo
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Adding this here to fake a correlation id for the purpose of generating events. Should not be used in a real situation.
            CorrelationIdContext.Current = new CorrelationIdContext { CorrelationId = Guid.NewGuid().ToString() };

            var host = new HostBuilder()
                .ConfigureHostConfiguration(config =>
                {
                    config.AddEnvironmentVariables(prefix: "ASPNETCORE_");
                })
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    config.AddJsonFile($"appsettings.json", true, true);
                    config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", true, true);
                    config.AddEnvironmentVariables();
                })
                .ConfigureLogging((hostContext, logging) =>
                {
                    var logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(hostContext.Configuration)
                        .Enrich.WithCorrelationId()
                        .CreateLogger();
                    logging.AddSerilog(logger);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddDefaultAWSOptions(hostContext.Configuration.GetAWSOptions());
                    services.AddAWSService<IAmazonSimpleNotificationService>();
                    services.AddAWSService<IAmazonSQS>();

                    // Adding this here to fake an identity for the purpose of generating events. Should not be used in a real situation.
                    services.AddSingleton<IIdentityProvider, IdentityProvider>();

                    services.AddMediatR(typeof(EventNotificationHandler).Assembly,
                        typeof(Event).Assembly,
                        typeof(Program).Assembly);

                    services.AddScoutEvents(hostContext.Configuration.GetSection("Events"));
                    services.AddAwsEventServices();
                    services.AddScoutEventCorrelationIdProvider();
                    services.AddScoped<IMessageProcessor, MessageProcessor>();

                    services.AddSingleton<LambdaFunction>();
                    services.AddHostedService<Producer>();
                    services.AddHostedService<Consumer>();
                })
                .Build();

            await host.RunAsync();
        }
    }

    public class IdentityProvider : IIdentityProvider
    {
        public Identity Current
        {
            get
            {
                return Identity.Parse($"0/User/{Guid.NewGuid()}");
            }
        }
    }
}
