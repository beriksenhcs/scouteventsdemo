﻿using System;
using System.IO;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using HCS.Scout.Core.Events;
using HCS.Scout.Core.Events.Aws;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;

namespace ScoutEventsDemo
{
    public class LambdaFunction
    {
        private static IServiceProvider _services;

        static LambdaFunction()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var serviceCollection = new ServiceCollection();

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environmentName}.json", true, true)
                .AddEnvironmentVariables();
            var configuration = configurationBuilder.Build();

            serviceCollection.AddLogging(logging =>
            {
                var logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .CreateLogger();
                logging.AddSerilog(logger);
            });

            serviceCollection.AddAwsEventServices();
            serviceCollection.AddScoutEventIdentityProvider();
            serviceCollection.AddScoutEventCorrelationIdProvider();
            serviceCollection.AddScoped<IMessageProcessor, MessageProcessor>();

            _services = serviceCollection.BuildServiceProvider();
        }

        private static readonly Action<Microsoft.Extensions.Logging.ILogger, string, Exception> _exceptionEncountered =
            LoggerMessage.Define<string>(LogLevel.Error, new EventId(400, "ExceptionEncountered"),
                "Error while attempting to handle message {message}");

        public async Task FunctionHandler(SQSEvent sqsEvent, ILambdaContext context)
        {
            using (var scope = _services.CreateScope())
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<LambdaFunction>>();
                logger.LogInformation($"Beginning to process {sqsEvent.Records.Count} records.");
                foreach (var record in sqsEvent.Records)
                {
                    using (logger.BeginScope("{sqsMessageId}", record.MessageId))
                    {
                        try
                        {
                            var messageProcessor = scope.ServiceProvider.GetRequiredService<IMessageProcessor>();
                            await messageProcessor.ProcessMessage(record.Body);
                        }
                        catch (Exception e)
                        {
                            var serializedRecord = JsonConvert.SerializeObject(record);
                            _exceptionEncountered(logger, serializedRecord, e);
                            throw;
                        }
                    }
                }
            }
        }
    }
}