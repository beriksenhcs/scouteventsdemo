﻿using System.Threading.Tasks;

namespace ScoutEventsDemo
{
    public interface IMessageProcessor
    {
        Task ProcessMessage(string message);
    }
}