﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Lambda.SQSEvents;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using static Amazon.Lambda.SQSEvents.SQSEvent;

namespace ScoutEventsDemo
{
    /// <summary>
    /// This is just a simple polling service that asks an SQS queue for messages and passes them off to a "lambda" function.
    /// In a real scenario, this portion would be implemented directly by Lambda.
    /// </summary>
    /// <seealso cref="Microsoft.Extensions.Hosting.BackgroundService" />
    public class Consumer : BackgroundService
    {
        private readonly IAmazonSQS _sqs;
        private readonly LambdaFunction _function;
        private readonly ILogger _logger;

        public Consumer(IAmazonSQS sqs, LambdaFunction function, ILogger<Consumer> logger)
        {
            _sqs = sqs ?? throw new ArgumentNullException(nameof(sqs));
            _function = function ?? throw new ArgumentNullException(nameof(function));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var queueUrl = "https://sqs.us-east-1.amazonaws.com/177554766653/braden-events-test";
            var request = new ReceiveMessageRequest
            {
                MaxNumberOfMessages = 1,
                MessageAttributeNames = { "All" },
                QueueUrl = queueUrl,
                WaitTimeSeconds = 20
            };
            ReceiveMessageResponse response;
            do
            {
                try
                {
                    response = await _sqs.ReceiveMessageAsync(request, cancellationToken);
                    if (response.HttpStatusCode == System.Net.HttpStatusCode.OK && response.Messages.Any())
                    {
                        var sqsEvent = new SQSEvent
                        {
                            Records = response.Messages.Select(m =>
                            {
                                return new SQSEvent.SQSMessage
                                {
                                    MessageId = m.MessageId,
                                    MessageAttributes = m.MessageAttributes.ToDictionary(x => x.Key, x => new MessageAttribute
                                    {
                                        StringValue = x.Value.StringValue,
                                        DataType = x.Value.DataType,
                                        BinaryListValues = x.Value.BinaryListValues,
                                        BinaryValue = x.Value.BinaryValue,
                                        StringListValues = x.Value.StringListValues
                                    }),
                                    Body = m.Body
                                };
                            }).ToList()
                        };
                        await _function.FunctionHandler(sqsEvent, null);
                        await _sqs.DeleteMessageBatchAsync(new DeleteMessageBatchRequest
                        {
                            Entries = response.Messages.Select((x, i) => new DeleteMessageBatchRequestEntry
                            {
                                Id = i.ToString(),
                                ReceiptHandle = x.ReceiptHandle
                            }).ToList(),
                            QueueUrl = queueUrl
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, e.Message);
                    throw;
                }
            }
            while (response != null && response.HttpStatusCode == System.Net.HttpStatusCode.OK && !cancellationToken.IsCancellationRequested);
        }
    }
}
